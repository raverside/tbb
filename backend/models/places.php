<?php 
namespace app\models;

use yii\db\ActiveRecord;

class Places extends ActiveRecord
{
    
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{places}}';
    }
    // declaring safe attributes
    public function rules()
	{
	    return [
	        [['name', 'description', 'image', 'sort'], 'safe'],
	        [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
	    ];
	}

	public function upload()
    {
        if ($this->validate()) {
            $this->image->saveAs('uploads/' . $this->image->baseName . '.' . $this->image->extension, false);
            return true;
        } else {
            return false;
        }
    }
}