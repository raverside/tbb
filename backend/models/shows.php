<?php 
namespace app\models;

use yii\db\ActiveRecord;

class Shows extends ActiveRecord
{
    
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{shows}}';
    }
    // declaring safe attributes
    public function rules()
	{
	    return [
	        [['name', 'description', 'image'], 'safe'],
	        [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
	    ];
	}

	public function upload()
    {
        if ($this->validate()) {
            $this->image->saveAs('uploads/' . $this->image->baseName . '.' . $this->image->extension, false);
            return true;
        } else {
            return false;
        }
    }
}