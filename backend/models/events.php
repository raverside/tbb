<?php 
namespace app\models;

use yii\db\ActiveRecord;

class Events extends ActiveRecord
{
    
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{events}}';
    }
    // declaring safe attributes
    public function rules()
	{
	    return [
	        [['show_id', 'place_id', 'date'], 'safe'],
	    ];
	}
}