<?php 
use yii\grid\GridView; 
use yii\helpers\Html;
?>

<a href="<?=\Yii::$app->request->BaseUrl?>/places/add" class="add-link">Create new place</a>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'name',
        [
        'attribute' => 'description', 
        'value' => function ($data) {
            return mb_strimwidth($data['description'], 0, 100, '...');
        }],
        [
            'attribute' => 'image',
            'format' => 'html',
            'label' => 'Image',
            'value' => function ($data) {
                if ($data['image']) { return Html::img(Yii::$app->request->BaseUrl.'/uploads/' . $data['image'],
                    ['class' => 'grid-img']);
            	}
            },
        ],
        'sort',
        [
            'attribute' => 'edit',
            'format' => 'html',
            'label' => 'Edit',
            'value' => function ($data) {
                return Html::a('Edit', Yii::$app->request->BaseUrl . '/places/edit?id=' . $data['id']);
            },
        ],
        [
            'attribute' => 'remove',
            'format' => 'html',
            'label' => 'Remove',
            'value' => function ($data) {
                return Html::a('Remove', Yii::$app->request->BaseUrl . '/places/remove?id=' . $data['id'], ['class' => 'js-remove']);
            },
        ],
    ],
]);
