<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'id' => 'add-form',
    'options' => ['class' => 'form-vertical', 'enctype' => 'multipart/form-data'],
]) ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'description')->textarea(['rows' => '6']) ?>
    <?= $form->field($model, 'image')->fileInput() ?>
    <?= $form->field($model, 'sort')->textInput(['type' => 'number']) ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
        <?= \yii\helpers\Html::a( 'Cancel', Yii::$app->request->referrer); ?>
    </div>
<?php ActiveForm::end() ?>