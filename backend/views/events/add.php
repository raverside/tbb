<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'id' => 'add-form',
    'options' => ['class' => 'form-vertical', 'enctype' => 'multipart/form-data'],
]) ?>
    <?=$form->field($model, 'show_id')->dropDownList($shows)->label('The Show') ?>
    <?=$form->field($model, 'place_id')->dropDownList($places)->label('The Place') ?>
    <?=$form->field($model, 'date')->widget(\yii\jui\DatePicker::classname(), [
	    //'language' => 'ru',
	    'dateFormat' => 'yyyy-MM-dd',
	    'options' => ['class' => 'form-control'],
	]) ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
        <?= \yii\helpers\Html::a( 'Cancel', Yii::$app->request->referrer); ?>
    </div>
<?php ActiveForm::end() ?>