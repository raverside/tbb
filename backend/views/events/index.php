<?php 
use yii\grid\GridView; 
use yii\helpers\Html;
use app\models\Shows;
use app\models\Places;
?>

<a href="<?=\Yii::$app->request->BaseUrl?>/events/add" class="add-link">Create new event</a>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'show_id',
            'format' => 'html',
            'label' => 'The Show',
            'value' => function ($data) {
                $show = Shows::findOne($data['show_id']);
                return $show ? $show->name : 'Unknown';
            },
        ],
        [
            'attribute' => 'place_id',
            'format' => 'html',
            'label' => 'The Place',
            'value' => function ($data) {
                $show = Places::findOne($data['place_id']);
                return $show ? $show->name : 'Unknown';
            },
        ],
        'date',

        [
                'attribute' => 'edit',
                'format' => 'html',
                'label' => 'Edit',
                'value' => function ($data) {
                    return Html::a('Edit', Yii::$app->request->BaseUrl . '/events/edit?id=' . $data['id']);
                },
        ],
        [
            'attribute' => 'remove',
            'format' => 'html',
            'label' => 'Remove',
            'value' => function ($data) {
                return Html::a('Remove', Yii::$app->request->BaseUrl . '/events/remove?id=' . $data['id'], ['class' => 'js-remove']);
            },
        ],
    ],
]);
