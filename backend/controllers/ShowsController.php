<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use app\models\Shows;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class ShowsController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
    	$dataProvider = new ActiveDataProvider([
		    'query' => Shows::find(),
		    'pagination' => [
		        'pageSize' => 10,
		    ],
		]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionAdd()
    {
    	if (Yii::$app->request->post()) {

    		$place = new Shows();
    		$place->attributes = Yii::$app->request->post()['Shows'];
    		$place->image = UploadedFile::getInstance($place, 'image');
            if (!$place->upload()) {
                return $this->redirect('add');
            }
    		$place->save();
    		return $this->redirect('index');

    	} else {

    		$model = new Shows;
    		return $this->render('add', ['model' => $model]);

    	}

    }

    public function actionEdit()
    {

    	if (Yii::$app->request->post()) {

    		$place = Shows::findOne(Yii::$app->request->get()['id']);
    		if ($place) {
    			$post = array_filter(Yii::$app->request->post()['Shows']);
	    		$place->attributes = $post;

	    		if (!empty(UploadedFile::getInstance($place, 'image'))) {
		    		$place->image = UploadedFile::getInstance($place, 'image');
		    		if(!empty($place->image) && $place->image->size !== 0) {
		            	$place->upload();
		        	}
	        	}
	    		$place->save();
	    		return $this->redirect('index');
    		}

    	} else {

    		$model = Shows::findOne(Yii::$app->request->get()['id']);
    		return $this->render('edit', ['model' => $model]);

    	}

    }

    public function actionRemove()
    {

    	if (Yii::$app->request->get()['id']) {
    		Shows::findOne(Yii::$app->request->get()['id'])->delete();
    		return $this->redirect('index');
    	}

    }

}
