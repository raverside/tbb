<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use app\models\Events;
use app\models\Shows;
use app\models\Places;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

/**
 * Site controller
 */
class EventsController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
    	$dataProvider = new ActiveDataProvider([
		    'query' => Events::find(),
		    'pagination' => [
		        'pageSize' => 10,
		    ],
		]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionAdd()
    {
    	if (Yii::$app->request->post() && Yii::$app->request->post()['Events']['date']) {

    		$event = new Events();
    		$event->attributes = Yii::$app->request->post()['Events'];

    		if ($event->save()) {
    		  return $this->redirect('index');
            } else {
                return $this->redirect('add');
            }

    	} else {

    		$model = new Events;
            $shows = ArrayHelper::map(Shows::find()->asArray()->all(), 'id', 'name');
            $places = ArrayHelper::map(Places::find()->asArray()->all(), 'id', 'name');

    		return $this->render('add', ['model' => $model, 'shows' => $shows, 'places' => $places]);

    	}

    }

    public function actionEdit()
    {

    	if (Yii::$app->request->post()) {

    		$event = Events::findOne(Yii::$app->request->get()['id']);
    		if ($event) {
    			$post = array_filter(Yii::$app->request->post()['Events']);
	    		$event->attributes = $post;

	    		$event->save();
	    		return $this->redirect('index');
    		}

    	} else {

    		$model = Events::findOne(Yii::$app->request->get()['id']);
            $shows = ArrayHelper::map(Shows::find()->asArray()->all(), 'id', 'name');
            $places = ArrayHelper::map(Places::find()->asArray()->all(), 'id', 'name');
    		return $this->render('edit', ['model' => $model, 'shows' => $shows, 'places' => $places]);

    	}

    }

    public function actionRemove()
    {

    	if (Yii::$app->request->get()['id']) {
    		Events::findOne(Yii::$app->request->get()['id'])->delete();
    		return $this->redirect('index');
    	}

    }

}
