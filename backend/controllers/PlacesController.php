<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use app\models\Places;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class PlacesController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
    	$dataProvider = new ActiveDataProvider([
		    'query' => Places::find(),
		    'pagination' => [
		        'pageSize' => 10,
		    ],
		]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionAdd()
    {
    	if (Yii::$app->request->post()) {

    		$place = new Places();
    		$place->attributes = Yii::$app->request->post()['Places'];
    		$place->image = UploadedFile::getInstance($place, 'image');
            if (!$place->upload()) {
                return $this->redirect('add');
            }
    		$place->save();
    		return $this->redirect('index');

    	} else {

    		$model = new Places;
    		return $this->render('add', ['model' => $model]);

    	}

    }

    public function actionEdit()
    {

    	if (Yii::$app->request->post()) {

    		$place = Places::findOne(Yii::$app->request->get()['id']);
    		if ($place) {
    			$post = array_filter(Yii::$app->request->post()['Places']);
	    		$place->attributes = $post;

	    		if (!empty(UploadedFile::getInstance($place, 'image'))) {
		    		$place->image = UploadedFile::getInstance($place, 'image');
		    		if(!empty($place->image) && $place->image->size !== 0) {
		            	$place->upload();
		        	}
	        	}
	    		$place->save();
	    		return $this->redirect('index');
    		}

    	} else {

    		$model = Places::findOne(Yii::$app->request->get()['id']);
    		return $this->render('edit', ['model' => $model]);

    	}

    }

    public function actionRemove()
    {

    	if (Yii::$app->request->get()['id']) {
    		Places::findOne(Yii::$app->request->get()['id'])->delete();
    		return $this->redirect('index');
    	}

    }

}
