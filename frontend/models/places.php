<?php 
namespace app\models;

use yii\db\ActiveRecord;

class Places extends ActiveRecord
{
    
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{places}}';
    }
    public function getEvents() {
        return $this->hasMany(Events::className(), ['id' => 'place_id']);
    }
    // declaring safe attributes
    public function rules()
	{
	    return [
	        [['name', 'description', 'image', 'sort'], 'safe'],
	    ];
	}

}