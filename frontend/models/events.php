<?php 
namespace app\models;

use yii\db\ActiveRecord;

class Events extends ActiveRecord
{
    
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{events}}';
    }
    public function getShows() {
        return $this->hasOne(Shows::className(), ['id' => 'show_id']);
    }
    public function getPlaces() {
        return $this->hasOne(Places::className(), ['id' => 'place_id']);
    }
    // declaring safe attributes
    public function rules()
	{
	    return [
	        [['show_id', 'place_id', 'date'], 'safe'],
            [['show_id', 'place_id', 'date'], 'required'],
	    ];
	}
}