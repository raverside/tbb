<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\models\Events;
use yii\data\Pagination;

/**
 * Site controller
 */
class EventsController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {	
    	$events = Events::find()->with(['shows', 'places'])->orderBy(['date' => 'DESC']);
    	$countEvents = clone $events;
    	$pages = new Pagination(['totalCount' => $countEvents->count(), 'pageSize'=>10]);
    	$events = $events->offset($pages->offset)
        ->limit(10)
        ->all();

        return $this->render('index', ['events' => $events, 'pages' => $pages]);
    }

}
