<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\models\Places;
use app\models\Events;
use yii\data\Pagination;

/**
 * Site controller
 */
class PlacesController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {	
    	$places = Places::find()->orderBy(['sort' => 'DESC'])->all();

        return $this->render('index', ['places' => $places]);
    }

    public function actionDetail()
    {
        $place = Places::find()->where(['id' => Yii::$app->request->get()['id']])->one();
        $events = Events::find()->where(['place_id' => Yii::$app->request->get()['id']])->with('shows');
        $countEvents = clone $events;
        $pages = new Pagination(['totalCount' => $countEvents->count(), 'pageSize'=>10]);
        $events = $events->offset($pages->offset)
        ->limit(10)
        ->all();

        return $this->render('detail', ['place' => $place, 'events' => $events, 'pages' => $pages]);
    }

}
