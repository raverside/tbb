<?php 
use yii\widgets\LinkPager;
$i = 0;
?>
<h1>Events <small>In the upcoming future</small></h1>
<div class="row">

<?php foreach ($events as $event) { 
	$i++;?>

	<div class="col-md-4 event-block">
        <a href="#">
        <?php if (!empty($event['shows']['image'])) { ?>
            <img class="img-responsive" src="<?=Yii::getAlias('@admin')?>/uploads/<?=$event['shows']['image']?>" alt="$event['shows']['name']" title="$event['shows']['name']">
        <?php } else if (!empty($event['places']['image'])) { ?> 
        	<img class="img-responsive" src="<?=Yii::getAlias('@admin')?>/uploads/<?=$event['places']['image']?>" alt="$event['places']['name']" title="$event['places']['name']">
        <?php } ?>
        </a>
        <h3>
            <a href="#"><?=$event['shows']['name']?></a>
        </h3>
        <p>Date: <small><?=$event['date']?></small></p>
        <p>Place: <small><?=$event['places']['name']?></small></p>
        <p><?=mb_strimwidth($event['places']['description'], 0, 100, '...')?></p>
    </div>

<?php if ($i % 3 == 0) {
	echo '</div><div class="row">';
	} 
} ?>
</div>
<div class="pagination">
<?php echo LinkPager::widget([
    'pagination' => $pages,
]);?>
</div>