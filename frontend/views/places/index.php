<div class="row text-center">
<?php $i = 0;
foreach ($places as $place) { 
	$i++;

?>

<div class="col-md-3">
    <div class="thumbnail">
        <img src="<?=Yii::getAlias('@admin')?>/uploads/<?=$place['image']?>" alt="" title="">
        <div class="caption">
            <h3><?=$place['name']?></h3>
            <p><?=$place['description']?></p>
            <p>
                <a href="<?=Yii::$app->homeUrl?>/places/detail?id=<?=$place['id']?>" class="btn btn-primary">View Details</a>
            </p>
        </div>
    </div>
</div>

<?php if ($i % 4 == 0) {
	echo '</div><div class="row text-center">';
	} 
}?>
</div>