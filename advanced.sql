-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 06, 2017 at 04:25 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `advanced`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `show_id` int(11) NOT NULL,
  `place_id` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `show_id`, `place_id`, `date`) VALUES
(3, 2, 13, '2017-03-07'),
(4, 4, 12, '2017-03-08'),
(5, 8, 13, '2017-03-24'),
(6, 4, 13, '2017-03-15'),
(7, 7, 12, '2017-03-23'),
(8, 6, 13, '2017-03-21'),
(9, 10, 13, '2017-03-17'),
(10, 10, 13, '2017-03-31'),
(11, 11, 12, '2017-03-31'),
(12, 9, 13, '2017-03-14'),
(13, 2, 12, '2017-03-22'),
(14, 2, 12, '2017-03-07');

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

CREATE TABLE `places` (
  `id` int(11) NOT NULL,
  `name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`id`, `name`, `description`, `image`, `sort`) VALUES
(12, 'Some place', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'placeholder.png', 1),
(13, 'Some other place', 'lorem ipsum overload', 'placeholder.png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `shows`
--

CREATE TABLE `shows` (
  `id` int(11) NOT NULL,
  `name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `shows`
--

INSERT INTO `shows` (`id`, `name`, `image`, `description`) VALUES
(2, 'Some show 1', 'placeholder.png', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque quis ante in neque rhoncus venenatis vehicula et magna. Mauris venenatis justo nec magna aliquet, malesuada sagittis enim rhoncus. Phasellus ac felis est. Integer ac mi quis augue scelerisque efficitur vel placerat dolor. Maecenas vestibulum eros a ligula tempus rutrum. Duis ultricies dapibus efficitur. Praesent sit amet eros ac turpis congue feugiat. Suspendisse in felis justo. Quisque eget felis ut velit mollis venenatis sit amet quis erat.\r\n\r\nAenean consectetur metus quam, sed fermentum urna vehicula id. Curabitur malesuada viverra molestie. Ut pharetra tristique elit, vitae gravida nulla. In sollicitudin libero vel consectetur vulputate. Donec id ipsum pellentesque, interdum nunc at, eleifend mauris. Sed lobortis lectus tempor mi ullamcorper semper. Sed eu vulputate ipsum. Nunc sodales ut est id euismod. Duis sit amet mi dapibus, fringilla velit eu, dictum nunc. Cras finibus sed nunc id finibus. Sed sodales suscipit tortor, at semper diam accumsan quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In efficitur sem nibh, posuere eleifend dui cursus vel. Donec et ante quis odio pellentesque gravida vitae eget augue. Nunc nulla nisl, dapibus convallis fringilla sagittis, tristique ac sem.'),
(3, 'Some show 2', 'placeholder.png', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque quis ante in neque rhoncus venenatis vehicula et magna. Mauris venenatis justo nec magna aliquet, malesuada sagittis enim rhoncus. Phasellus ac felis est. Integer ac mi quis augue scelerisque efficitur vel placerat dolor. Maecenas vestibulum eros a ligula tempus rutrum. Duis ultricies dapibus efficitur. Praesent sit amet eros ac turpis congue feugiat. Suspendisse in felis justo. Quisque eget felis ut velit mollis venenatis sit amet quis erat.\r\n\r\nAenean consectetur metus quam, sed fermentum urna vehicula id. Curabitur malesuada viverra molestie. Ut pharetra tristique elit, vitae gravida nulla. In sollicitudin libero vel consectetur vulputate. Donec id ipsum pellentesque, interdum nunc at, eleifend mauris. Sed lobortis lectus tempor mi ullamcorper semper. Sed eu vulputate ipsum. Nunc sodales ut est id euismod. Duis sit amet mi dapibus, fringilla velit eu, dictum nunc. Cras finibus sed nunc id finibus. Sed sodales suscipit tortor, at semper diam accumsan quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In efficitur sem nibh, posuere eleifend dui cursus vel. Donec et ante quis odio pellentesque gravida vitae eget augue. Nunc nulla nisl, dapibus convallis fringilla sagittis, tristique ac sem.'),
(4, 'Some show 3', 'placeholder.png', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque quis ante in neque rhoncus venenatis vehicula et magna. Mauris venenatis justo nec magna aliquet, malesuada sagittis enim rhoncus. Phasellus ac felis est. Integer ac mi quis augue scelerisque efficitur vel placerat dolor. Maecenas vestibulum eros a ligula tempus rutrum. Duis ultricies dapibus efficitur. Praesent sit amet eros ac turpis congue feugiat. Suspendisse in felis justo. Quisque eget felis ut velit mollis venenatis sit amet quis erat.\r\n\r\nAenean consectetur metus quam, sed fermentum urna vehicula id. Curabitur malesuada viverra molestie. Ut pharetra tristique elit, vitae gravida nulla. In sollicitudin libero vel consectetur vulputate. Donec id ipsum pellentesque, interdum nunc at, eleifend mauris. Sed lobortis lectus tempor mi ullamcorper semper. Sed eu vulputate ipsum. Nunc sodales ut est id euismod. Duis sit amet mi dapibus, fringilla velit eu, dictum nunc. Cras finibus sed nunc id finibus. Sed sodales suscipit tortor, at semper diam accumsan quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In efficitur sem nibh, posuere eleifend dui cursus vel. Donec et ante quis odio pellentesque gravida vitae eget augue. Nunc nulla nisl, dapibus convallis fringilla sagittis, tristique ac sem.'),
(5, 'Some show 4', 'placeholder.png', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque quis ante in neque rhoncus venenatis vehicula et magna. Mauris venenatis justo nec magna aliquet, malesuada sagittis enim rhoncus. Phasellus ac felis est. Integer ac mi quis augue scelerisque efficitur vel placerat dolor. Maecenas vestibulum eros a ligula tempus rutrum. Duis ultricies dapibus efficitur. Praesent sit amet eros ac turpis congue feugiat. Suspendisse in felis justo. Quisque eget felis ut velit mollis venenatis sit amet quis erat.\r\n\r\nAenean consectetur metus quam, sed fermentum urna vehicula id. Curabitur malesuada viverra molestie. Ut pharetra tristique elit, vitae gravida nulla. In sollicitudin libero vel consectetur vulputate. Donec id ipsum pellentesque, interdum nunc at, eleifend mauris. Sed lobortis lectus tempor mi ullamcorper semper. Sed eu vulputate ipsum. Nunc sodales ut est id euismod. Duis sit amet mi dapibus, fringilla velit eu, dictum nunc. Cras finibus sed nunc id finibus. Sed sodales suscipit tortor, at semper diam accumsan quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In efficitur sem nibh, posuere eleifend dui cursus vel. Donec et ante quis odio pellentesque gravida vitae eget augue. Nunc nulla nisl, dapibus convallis fringilla sagittis, tristique ac sem.'),
(6, 'Some show 5', 'placeholder.png', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque quis ante in neque rhoncus venenatis vehicula et magna. Mauris venenatis justo nec magna aliquet, malesuada sagittis enim rhoncus. Phasellus ac felis est. Integer ac mi quis augue scelerisque efficitur vel placerat dolor. Maecenas vestibulum eros a ligula tempus rutrum. Duis ultricies dapibus efficitur. Praesent sit amet eros ac turpis congue feugiat. Suspendisse in felis justo. Quisque eget felis ut velit mollis venenatis sit amet quis erat.\r\n\r\nAenean consectetur metus quam, sed fermentum urna vehicula id. Curabitur malesuada viverra molestie. Ut pharetra tristique elit, vitae gravida nulla. In sollicitudin libero vel consectetur vulputate. Donec id ipsum pellentesque, interdum nunc at, eleifend mauris. Sed lobortis lectus tempor mi ullamcorper semper. Sed eu vulputate ipsum. Nunc sodales ut est id euismod. Duis sit amet mi dapibus, fringilla velit eu, dictum nunc. Cras finibus sed nunc id finibus. Sed sodales suscipit tortor, at semper diam accumsan quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In efficitur sem nibh, posuere eleifend dui cursus vel. Donec et ante quis odio pellentesque gravida vitae eget augue. Nunc nulla nisl, dapibus convallis fringilla sagittis, tristique ac sem.'),
(7, 'Some show 6', 'placeholder.png', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque quis ante in neque rhoncus venenatis vehicula et magna. Mauris venenatis justo nec magna aliquet, malesuada sagittis enim rhoncus. Phasellus ac felis est. Integer ac mi quis augue scelerisque efficitur vel placerat dolor. Maecenas vestibulum eros a ligula tempus rutrum. Duis ultricies dapibus efficitur. Praesent sit amet eros ac turpis congue feugiat. Suspendisse in felis justo. Quisque eget felis ut velit mollis venenatis sit amet quis erat.\r\n\r\nAenean consectetur metus quam, sed fermentum urna vehicula id. Curabitur malesuada viverra molestie. Ut pharetra tristique elit, vitae gravida nulla. In sollicitudin libero vel consectetur vulputate. Donec id ipsum pellentesque, interdum nunc at, eleifend mauris. Sed lobortis lectus tempor mi ullamcorper semper. Sed eu vulputate ipsum. Nunc sodales ut est id euismod. Duis sit amet mi dapibus, fringilla velit eu, dictum nunc. Cras finibus sed nunc id finibus. Sed sodales suscipit tortor, at semper diam accumsan quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In efficitur sem nibh, posuere eleifend dui cursus vel. Donec et ante quis odio pellentesque gravida vitae eget augue. Nunc nulla nisl, dapibus convallis fringilla sagittis, tristique ac sem.'),
(8, 'Some show 7', 'placeholder.png', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque quis ante in neque rhoncus venenatis vehicula et magna. Mauris venenatis justo nec magna aliquet, malesuada sagittis enim rhoncus. Phasellus ac felis est. Integer ac mi quis augue scelerisque efficitur vel placerat dolor. Maecenas vestibulum eros a ligula tempus rutrum. Duis ultricies dapibus efficitur. Praesent sit amet eros ac turpis congue feugiat. Suspendisse in felis justo. Quisque eget felis ut velit mollis venenatis sit amet quis erat.\r\n\r\nAenean consectetur metus quam, sed fermentum urna vehicula id. Curabitur malesuada viverra molestie. Ut pharetra tristique elit, vitae gravida nulla. In sollicitudin libero vel consectetur vulputate. Donec id ipsum pellentesque, interdum nunc at, eleifend mauris. Sed lobortis lectus tempor mi ullamcorper semper. Sed eu vulputate ipsum. Nunc sodales ut est id euismod. Duis sit amet mi dapibus, fringilla velit eu, dictum nunc. Cras finibus sed nunc id finibus. Sed sodales suscipit tortor, at semper diam accumsan quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In efficitur sem nibh, posuere eleifend dui cursus vel. Donec et ante quis odio pellentesque gravida vitae eget augue. Nunc nulla nisl, dapibus convallis fringilla sagittis, tristique ac sem.'),
(9, 'Some show 8', 'placeholder.png', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque quis ante in neque rhoncus venenatis vehicula et magna. Mauris venenatis justo nec magna aliquet, malesuada sagittis enim rhoncus. Phasellus ac felis est. Integer ac mi quis augue scelerisque efficitur vel placerat dolor. Maecenas vestibulum eros a ligula tempus rutrum. Duis ultricies dapibus efficitur. Praesent sit amet eros ac turpis congue feugiat. Suspendisse in felis justo. Quisque eget felis ut velit mollis venenatis sit amet quis erat.\r\n\r\nAenean consectetur metus quam, sed fermentum urna vehicula id. Curabitur malesuada viverra molestie. Ut pharetra tristique elit, vitae gravida nulla. In sollicitudin libero vel consectetur vulputate. Donec id ipsum pellentesque, interdum nunc at, eleifend mauris. Sed lobortis lectus tempor mi ullamcorper semper. Sed eu vulputate ipsum. Nunc sodales ut est id euismod. Duis sit amet mi dapibus, fringilla velit eu, dictum nunc. Cras finibus sed nunc id finibus. Sed sodales suscipit tortor, at semper diam accumsan quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In efficitur sem nibh, posuere eleifend dui cursus vel. Donec et ante quis odio pellentesque gravida vitae eget augue. Nunc nulla nisl, dapibus convallis fringilla sagittis, tristique ac sem.'),
(10, 'Some show 9', 'placeholder.png', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque quis ante in neque rhoncus venenatis vehicula et magna. Mauris venenatis justo nec magna aliquet, malesuada sagittis enim rhoncus. Phasellus ac felis est. Integer ac mi quis augue scelerisque efficitur vel placerat dolor. Maecenas vestibulum eros a ligula tempus rutrum. Duis ultricies dapibus efficitur. Praesent sit amet eros ac turpis congue feugiat. Suspendisse in felis justo. Quisque eget felis ut velit mollis venenatis sit amet quis erat.\r\n\r\nAenean consectetur metus quam, sed fermentum urna vehicula id. Curabitur malesuada viverra molestie. Ut pharetra tristique elit, vitae gravida nulla. In sollicitudin libero vel consectetur vulputate. Donec id ipsum pellentesque, interdum nunc at, eleifend mauris. Sed lobortis lectus tempor mi ullamcorper semper. Sed eu vulputate ipsum. Nunc sodales ut est id euismod. Duis sit amet mi dapibus, fringilla velit eu, dictum nunc. Cras finibus sed nunc id finibus. Sed sodales suscipit tortor, at semper diam accumsan quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In efficitur sem nibh, posuere eleifend dui cursus vel. Donec et ante quis odio pellentesque gravida vitae eget augue. Nunc nulla nisl, dapibus convallis fringilla sagittis, tristique ac sem.'),
(11, 'Some show 10', 'placeholder.png', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque quis ante in neque rhoncus venenatis vehicula et magna. Mauris venenatis justo nec magna aliquet, malesuada sagittis enim rhoncus. Phasellus ac felis est. Integer ac mi quis augue scelerisque efficitur vel placerat dolor. Maecenas vestibulum eros a ligula tempus rutrum. Duis ultricies dapibus efficitur. Praesent sit amet eros ac turpis congue feugiat. Suspendisse in felis justo. Quisque eget felis ut velit mollis venenatis sit amet quis erat.\r\n\r\nAenean consectetur metus quam, sed fermentum urna vehicula id. Curabitur malesuada viverra molestie. Ut pharetra tristique elit, vitae gravida nulla. In sollicitudin libero vel consectetur vulputate. Donec id ipsum pellentesque, interdum nunc at, eleifend mauris. Sed lobortis lectus tempor mi ullamcorper semper. Sed eu vulputate ipsum. Nunc sodales ut est id euismod. Duis sit amet mi dapibus, fringilla velit eu, dictum nunc. Cras finibus sed nunc id finibus. Sed sodales suscipit tortor, at semper diam accumsan quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In efficitur sem nibh, posuere eleifend dui cursus vel. Donec et ante quis odio pellentesque gravida vitae eget augue. Nunc nulla nisl, dapibus convallis fringilla sagittis, tristique ac sem.'),
(12, 'Some show 11', 'placeholder.png', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque quis ante in neque rhoncus venenatis vehicula et magna. Mauris venenatis justo nec magna aliquet, malesuada sagittis enim rhoncus. Phasellus ac felis est. Integer ac mi quis augue scelerisque efficitur vel placerat dolor. Maecenas vestibulum eros a ligula tempus rutrum. Duis ultricies dapibus efficitur. Praesent sit amet eros ac turpis congue feugiat. Suspendisse in felis justo. Quisque eget felis ut velit mollis venenatis sit amet quis erat.\r\n\r\nAenean consectetur metus quam, sed fermentum urna vehicula id. Curabitur malesuada viverra molestie. Ut pharetra tristique elit, vitae gravida nulla. In sollicitudin libero vel consectetur vulputate. Donec id ipsum pellentesque, interdum nunc at, eleifend mauris. Sed lobortis lectus tempor mi ullamcorper semper. Sed eu vulputate ipsum. Nunc sodales ut est id euismod. Duis sit amet mi dapibus, fringilla velit eu, dictum nunc. Cras finibus sed nunc id finibus. Sed sodales suscipit tortor, at semper diam accumsan quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In efficitur sem nibh, posuere eleifend dui cursus vel. Donec et ante quis odio pellentesque gravida vitae eget augue. Nunc nulla nisl, dapibus convallis fringilla sagittis, tristique ac sem.');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `password_hash` text NOT NULL,
  `status` int(11) NOT NULL,
  `email` text NOT NULL,
  `auth_key` text NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password_hash`, `status`, `email`, `auth_key`, `created_at`, `updated_at`) VALUES
(5, 'admin1', '$2y$13$ujzMmwCxw2qiKFCNGsHJ5eogRVlqjH1GzYaMZIKoe8DoDKD5EluwW', 10, 'r4verside@gmail.com', 'zRh1M4gVrfFtI54vOxbLIupHbf2LdDcC', 1488730900, 1488730900);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shows`
--
ALTER TABLE `shows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `places`
--
ALTER TABLE `places`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `shows`
--
ALTER TABLE `shows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
